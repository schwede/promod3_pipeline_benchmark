# promod3_pipeline_benchmark

This repository contains code and data to reproduce the figures and tables 
from the benchmarking efforts in the manuscript: 
"ProMod3 - A versatile homology modelling toolbox".

The *modelling*, *loop*, *sidechain* and *other* directories contain detailed
instructions on how to reproduce the numbers and figures.

You need an OpenStructure and ProMod3 installation to run the scripts, 
more specifically you need the *ost* and *pm* executables in your path.
Furthermore you need the numpy, scipy and matplotlib Python modules. 

You may want to try a ProMod3 Singularity container
(https://openstructure.org/promod3/3.1/container/singularity) which should
contain the software required to execute most of the scripts in this repository.

