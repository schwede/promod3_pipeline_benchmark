Scripts and data to reproduce the homology modelling accuracy benchmark.

The whole benchmark is a three step process:

1. Fetch Benchmark Set
2. Modelling with ProMod3 and MODELLER
3. Evaluation

Fetch Benchmark Set
-------------------

We suggest to use the already provided benchmark set as you need a running 
SWISS-MODEL instance which is not publicly available. The steps are nevertheless 
documented here for internal reference.

- Download raw modelling evaluation data from CAMEO. Data for the last three 
  months is available here: https://www.cameo3d.org/sp/3-months/
  Adapt the **eval_dir**, **start_date** and **end_date** variables in 
  fetch_cameo_targets.py and run the script:
  `python fetch_cameo_targets.py`
- Templates, alignments and sequence profiles are fetched from a SWISS-MODEL
  server which was registered to CAMEO in the relevant time range and performed
  template searches accordingly. 
  Adapt the **sm_project_dir** variable in fetch_templates.py and run the script
  using the SWISS-MODEL executable:
  `sm fetch_templates.py`


Modelling with ProMod3 and MODELLER
-----------------------------------

- Given a working ProMod3 installation (i.e. pm executable in your path), 
  you can execute build_promod_models.py with the ProMod3 executable and the 
  created models are saved in the promod_models directory.
  `pm build_promod_models.py`
- You need your own MODELLER installation to build the according models. Make
  sure that you can execute `from modeller import *` with your python 
  executable. build_modeller_models.py builds and saves models to the 
  modeller_models directory:
  `python build_modeller_models.py`

Modelling with ProModII
-----------------------

- ProModII is considered legacy software and only available on our internal 
  systems. We provide the resulting models in this repository for further 
  processing. Only 169 modelling runs successfully produced a result.
  After setting up the environment (loading ProModII/3.70 and 
  ProMod3/3.1.1-foss-2018b-Python-3.6.6-OST-2.1.0 modules), the models have 
  been built by calling:
  `pm build_promodII_models.py`


Evaluation
----------

- You need to score all models using lDDT for the general model accuracy 
  as well as Molprobity to assess stereochemistry. Given a working ProMod3 
  installation, you also have a working ost executable to calculate lDDT.
  The ost executable needs to be in your path.
  The biggest challenge here is to get a working Molprobity installation 
  using the information on https://github.com/rlabduke/MolProbity. In case of 
  trouble, get in touch with the people from the Richardson Lab at Duke. 
  Alternatively, get in touch with the Schwede lab at the Biozentrum as they 
  can provide you a Singularity container running Molprobity. 
  Whatever your installation is, you need to adapt the command to call 
  Molprobity in score_models.py. Running score_models.py will fetch the models 
  in promod_models/modeller_models and evaluate them given the references in 
  cameo_benchmark. The results are dumped as 
  promod_scores.json/modeller_scores.json:
  `python score_models.py`
- Plots and some key values based on the score json files are generated with:
  `python do_plots.py`
- For the additional evaluation with ProModII execute:
  `python do_plots_promodII`
 
