import os
import subprocess
import json

# Command to execute molprobity on your system (Installation instructions:
# https://github.com/rlabduke/MolProbity). No matter how long
# the command, the last element (here marked with None) is the path to
# the model to process.
# The following requires Singularity and a container (molprobity.img) 
# with Molprobity installed as an app. Alternatively you can install 
# molprobity natively and adapt the command accordingly.
molprobity_cmd = ['singularity', 'run', '--app', 'molprobity', 
                  'molprobity.img', None]

def GetStringValue(my_line):
  return my_line.split('=')[1].strip().split(' ')[0]

def ParseMolprobityOut(sout):
    result = dict()
    first_found = False
    lines = sout.splitlines()
    for line in lines:
        my_line = line.strip().lower()
        if my_line.startswith("ramachandran outliers"):
            first_found = True
            result['Ramachandran outliers'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('favored'):
            result['Ramachandran favored'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('rotamer outliers'):
            result['Rotamer outliers'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('clashscore'):
            result['Clashscore'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('c-beta deviations'):
            result['C-beta deviations'] = int(GetStringValue(line))
        elif first_found and my_line.startswith('molprobity score'):
            result['MolProbity score'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('rms(bonds)'):
            result['RMS(bonds)'] = float(GetStringValue(line))
        elif first_found and my_line.startswith('rms(angles)'):
            result['RMS(angles)'] = float(GetStringValue(line))
    return result

def DoIt(model_dir, target_dir, molprobity_cmd):
    results = dict()
    model_files = os.listdir(model_dir)
    for mf in model_files:
        per_model_scores = dict()
        target_path = os.path.join(target_dir, mf)
        model_path = os.path.join(model_dir, mf)
        cmd = ['ost', 'compare-structures', '--model', model_path, 
               '--reference', target_path, '--output', 'scratch/scores.json',
               '--lddt', '--inclusion-radius', '15.0',
               '--molck', '--remove', 'oxt', 'hyd', 'unk', 'nonstd', '--clean-element-column',
               '--map-nonstandard-residues', '--structural-checks', 
               '--bond-tolerance', '15.0', '--angle-tolerance', '15.0',
               '--consistency-checks', '--residue-number-alignment']
        try:
            subprocess.run(cmd)
            with open('scratch/scores.json') as fh:
                s = json.load(fh)
            lddt = s['result'][mf][mf]['lddt']['single_chain_lddt'][0]['global_score']
        except:
            # potential fail if model is only short peptide
            print('failed for ', mf, 'skip...')
            continue
        per_model_scores['lddt'] = lddt

        molprobity_cmd[-1] = model_path
        p = subprocess.run(molprobity_cmd, stdout=subprocess.PIPE, 
                           stderr=subprocess.PIPE)
        result = ParseMolprobityOut(p.stdout.decode())
        per_model_scores['MolProbity score'] = result['MolProbity score']
        per_model_scores['Ramachandran outliers'] = result['Ramachandran outliers']
        per_model_scores['Ramachandran favored'] = result['Ramachandran favored']
        per_model_scores['Clashscore'] = result['Clashscore']
        per_model_scores['Rotamer outliers'] = result['Rotamer outliers']
        results[mf.split('.')[0]] = per_model_scores

    return results

results = DoIt('promod_models', 'cameo_benchmark', molprobity_cmd)
with open('promod_scores.json', 'w') as fh:
    json.dump(results, fh)

results = DoIt('modeller_models', 'cameo_benchmark', molprobity_cmd)
with open('modeller_scores.json', 'w') as fh:
    json.dump(results, fh)

results = DoIt('promodII_models', 'cameo_benchmark', molprobity_cmd)
with open('promodII_scores.json', 'w') as fh:
    json.dump(results, fh)

