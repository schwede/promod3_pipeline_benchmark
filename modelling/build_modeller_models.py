import os
import time
from ost import io
from ost import seq
from modeller import *
from modeller.automodel import *


in_dir = 'cameo_benchmark'
out_dir = "modeller_models"
scratch_dir = "scratch"

if not os.path.exists(out_dir):
    os.makedirs(out_dir)

if not os.path.exists(scratch_dir):
    os.makedirs(scratch_dir)

targets = set([f[:6] for f in os.listdir(in_dir)])

# Calling MODELLER requires some preparation, e.g. prepare alignment in PIR
# format, cut alignment to avoid unrealistic terminal tails etc.
# What we're interested in is raw modelling time by MODELLER. 
# This is returned by the ExecuteModeller function.
modelling_time = 0.0


def ExecuteModeller(aln, out_path, scratch_dir):

    # we first go into the modeller_scratch directory as we need to dump some
    # files
    orig_cwd = os.getcwd()
    os.chdir(scratch_dir)

    tpl_view = aln.GetSequence(1).GetAttachedView()

    # let's chop the alignment, so we enforce modelling of only the stretch 
    # covered by the template...
    start_idx = None
    start_rnum = 1
    end_idx = None
    for i, c in enumerate(aln):
        if c[1] != "-":
            start_idx = i
            break
        if c[0] != '-':
            start_rnum += 1
    for i in range(len(aln)):
        current_idx = len(aln) - 1 - i
        c = aln[current_idx]
        if c[1] != "-":
            end_idx = current_idx
            break
    target_seq = str(aln.GetSequence(0))[start_idx : end_idx + 1]
    template_seq = str(aln.GetSequence(1))[start_idx : end_idx + 1]
    target_seq = seq.CreateSequence("A", target_seq)
    template_seq = seq.CreateSequence("A", template_seq)
    template_seq.AttachView(tpl_view)
    sub_aln = seq.CreateAlignment()
    sub_aln.AddSequence(target_seq)
    sub_aln.AddSequence(template_seq)

    # write template and PIR alignment for MODELLER
    io.SavePDB(tpl_view, "template.pdb")
    with open("aln.ali", "w") as fh:
        fh.write(">P1;template")
        fh.write("\n")
        temp = ["structureX", "template", 
                str(tpl_view.residues[0].GetNumber().GetNum()),
                tpl_view.chains[0].GetName(), 
                str(tpl_view.residues[-1].GetNumber().GetNum()),
                tpl_view.chains[0].GetName(), "undefined", "undefined", "-1.0",
                "-1.0"]
        fh.write(":".join(temp))
        fh.write("\n")
        fh.write(sub_aln.GetSequence(1).GetString())
        fh.write("*")
        fh.write("\n")
        fh.write(">P1;target")
        fh.write("\n")
        temp = ["sequence", "target", str(1), "A", 
                str(len(sub_aln.GetSequence(0).GetGaplessString())), "A", " ",
                " ", "0.0", "0.0"]
        fh.write(":".join(temp))
        fh.write("\n")
        fh.write(sub_aln.GetSequence(0).GetString())
        fh.write("*")

    # GOGOGO
    modelling_start_time = time.time()
    env = environ()
    a = automodel(env, alnfile="aln.ali", knowns="template", sequence="target")
    a.starting_model = 1
    a.ending_model = 1
    a.make()
    modeller_out = a.outputs[0]
    modelling_time = time.time() - modelling_start_time

    # the model requires renumbering to match the initial target sequence
    model = io.LoadPDB(modeller_out["name"])
    ed = model.EditXCS()
    ed.RenumberChain(model.chains[0], start_rnum, True)

    # go back to original working dir and dump model
    os.chdir(orig_cwd)
    io.SavePDB(model, out_path)

    return modelling_time


for t in targets:
    tpl_path = os.path.join(in_dir, t + "_tpl.pdb")
    aln_path = os.path.join(in_dir, t + "_aln.fasta")
    out_path = os.path.join(out_dir, t + ".pdb")
    if os.path.exists(tpl_path) and os.path.exists(aln_path):
        aln = io.LoadAlignment(aln_path)
        template = io.LoadPDB(tpl_path).CreateFullView()
        aln.AttachView(1, template)
        modelling_time += ExecuteModeller(aln, out_path, scratch_dir)
    
print("full modelling time: ", modelling_time)
