import os
import sys
import ost
from sm.pipeline import ProjectFactory

sm_project_dir = '<PATH_TO_SWISSMODEL_PROJECT_DIR>'

# iterate over all SWISS-MODEL projects and store for each target sequence
# the according project path
sm_projects = dict() 
first_level_dirs = os.listdir(sm_project_dir)
for fld in first_level_dirs:
    second_level_dirs = os.listdir(os.path.join(sm_project_dir, fld))
    for sld in second_level_dirs:
        third_level_dirs = os.listdir(os.path.join(sm_project_dir, fld, sld))
        for tld in third_level_dirs:
            prj_path = os.path.join(sm_project_dir, fld, sld, tld)
            fasta_path = os.path.join(prj_path, "target.fasta")
            if not os.path.exists(fasta_path):
                print("project without target seq!")
                continue # Its not there
            target_sequence = ost.io.LoadSequence(fasta_path).GetString()
            sm_projects[target_sequence] = prj_path

# Get compound library for cleanup operations. Asumes OpenStructure to be compiled
# with compound library
compound_lib = ost.conop.GetDefaultLib()

for f in os.listdir('cameo_benchmark'):
    if f.endswith('.fasta') and len(f.split('.')[0]) == 6:
        trg_id = f[:6]
        trg_seq = str(ost.io.LoadSequence(os.path.join('cameo_benchmark', f)))
        if trg_seq in sm_projects:
            print('processing', trg_id)
            prj = ProjectFactory.FromName(sm_projects[trg_seq])

            # search for template with best hhblits evalue
            tpls = prj.GetTemplates()
            best_evalue = sys.maxsize
            best_tpl = None
            for tpl in tpls:
                if tpl.found_by != 'HHblits':
                    continue
                # hhblits parameters are stored in the tpl comments
                comment_str = tpl.comment
                data = comment_str.split(',')
                evalue = None
                for item in data:
                    if item.strip().startswith('e_value='):
                        evalue = float(item.split('=')[1])
                if evalue == None:
                    raise RuntimeError('could not extract evalue')
                if evalue < best_evalue:
                    best_evalue = evalue
                    best_tpl = tpl
            if best_tpl == None:
                print('Couldnt find any hhblits tpl for', trg_id, 'skip...')
                continue

            # extract template structure and alignment from smtl
            bio_unit = prj.smtl.Get(best_tpl.pdb_id, best_tpl.assembly_id)
            ch = bio_unit.GetChainByName(best_tpl.chain_name)
            aln = ch.ToAtomSeqAlignment(best_tpl.alignment)
            tpl_fn = prj.model_repos.FilenameForModel(best_tpl.pdb_id,
                                                      best_tpl.assembly_id,
                                                      best_tpl.chain_name)
            template_structure = io.LoadPDB(tpl_fn)

            # Map non standard residues (e.g. MSE to MET or MLY to LYS)
            # Check that we still have the same number of residues and that
            # every residue still has at least one atom. If this is not the
            # case, this would introduce an alignment shift.
            n_residues = len(template_structure.residues)
            ost.mol.alg.MapNonStandardResidues(template_structure, compound_lib, 
                                               False)
            if len(template_structure.residues) != n_residues:
                raise ValueError("Failed in template cleanup")
            for r in template_structure.residues:
                if len(r.atoms) == 0:
                    raise ValueError("Failed in template cleanup")

            # we don't want to deal with sequence offsets later on so let's just
            # extract the part of the template which is relevant
            aln.AttachView(1, template_structure.CreateFullView())
            for col in aln:
                if col[1] != '-':
                    r = col.GetResidue(1)
                    r.SetIntProp('aligned', 1)
            aligned_template = template_structure.Select('graligned:0=1')

            # save stuff
            out_path = os.path.join('cameo_benchmark', trg_id + "_tpl.pdb")
            ost.io.SavePDB(aligned_template, out_path)
            out_path = os.path.join('cameo_benchmark', trg_id + "_aln.fasta")
            ost.io.SaveAlignment(aln, out_path)
            out_path = os.path.join('cameo_benchmark', trg_id + '_profile.hhm')
            os.system('cp %s %s'%(prj.hhm_filename, out_path))
        else:
            print("could not find sm project for ", trg_id)

