import os
import datetime

testset_dir = 'cameo_benchmark'
eval_dir = '<PATH_TO_CAMEO_MODELING_EVAL_RAW_DATA>' # raw data available from
                                                    # cameo3d.org. 
start_date = datetime.date(2020, 03, 28)
end_date = datetime.date(2020, 06, 13)

for week in [w for w in os.listdir(eval_dir) if len(w.split('.')) == 3]:
    current_date = datetime.date(int(week.split('.')[0]), 
                                 int(week.split('.')[1]), 
                                 int(week.split('.')[2]))
    if current_date >= start_date and current_date <= end_date: 
        print("processing targets of week", week)
        week_dir = os.path.join(eval_dir, week)
        for target in os.listdir(week_dir):
            pdb_in = os.path.join(week_dir, target, 'target.pdb')
            fasta_in = os.path.join(week_dir, target, 'target.fasta')
            if os.path.exists(pdb_in) and os.path.exists(fasta_in):
                pdb_out = os.path.join(testset_dir, target + '.pdb')
                fasta_out = os.path.join(testset_dir, target + '.fasta')
                os.system('cp %s %s'%(pdb_in, pdb_out)) 
                os.system('cp %s %s'%(fasta_in, fasta_out))        
            else:
                print('failed to get data for', week, target)

