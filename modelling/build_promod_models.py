import os
import time
import subprocess

in_dir = 'cameo_benchmark'
out_dir = 'promod_models'

if not os.path.exists(out_dir):
  os.makedirs(out_dir)

targets = set([f[:6] for f in os.listdir(in_dir)])
start_time = time.time()
for t in targets:
    tpl_path = os.path.join(in_dir, t + "_tpl.pdb")
    aln_path = os.path.join(in_dir, t + "_aln.fasta")
    prof_path = os.path.join(in_dir, t + "_profile.hhm")
    out_path = os.path.join(out_dir, t + ".pdb")
    if os.path.exists(tpl_path) and os.path.exists(aln_path) and \
       os.path.exists(prof_path):
        cmd = ['pm', 'build-model', '-p', tpl_path, '-f', aln_path, 
               '-s', prof_path, '-o', out_path]
        p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(p.stdout.decode())
        print(p.stderr.decode())

print("full modelling time: ", time.time() - start_time)

