"""Run ProModII as modelling engine"""
import os
import time
import subprocess
from ost import io
from ost import seq
from ost import mol
import promod3


def renumber_chains(seqres, ent, offset):
    """Postprocessing step to make sure, that all chains are correctly numbered.
    Removed parts at the beginning or end are allowed.
    But if there are modelling gaps, the numbering gets messed up!
    """
    edx = ent.EditXCS()
    chn = ent.chains[0]
    rcaln = seq.alg.AlignToSEQRES(chn, seqres[offset[chn.name] :], validate=False)
    aligned_sequence = str(rcaln.FindSequence("atoms"))
    start = -1
    for i, aligned_aa in enumerate(aligned_sequence):
        if aligned_aa != "-":
            start = i + 1
            break
    if start != -1:
        start += offset[chn.name]
        edx.RenumberChain(chn, start, False)
    else:
        raise RuntimeError("Failed to renumber chain!")


def rename_chains(old_ent, names):
    """Fix ProMod's (sometimes) weird naming of chains."""
    new_ent = mol.CreateEntity()
    edx = new_ent.EditXCS()
    if len(names) != old_ent.chain_count:
        raise RuntimeError(
            "Cannot rename model's chains: "
            "different number of chains from template"
        )
    for chn, name in zip(old_ent.chains, names):
        edx.InsertChain(name, chn, deep=True)
    edx.UpdateICS()
    io.processor.Process(new_ent)
    return new_ent


def write_batch_file(scratch_path, target_sequence):
    """Create the ProModII batch file"""
    with open(os.path.join(scratch_path, "model.batch.1"), "w") as btch_fh:
        btch_fh.write("#FOLDFITMODE\n")
        btch_fh.write("%raw-model.pdb\n")
        btch_fh.write("#RENUMBER 1\n")
        btch_fh.write(">TARGET\n")
        btch_fh.write("%s;\n" % target_sequence)
        btch_fh.write(".\n")
        btch_fh.write("%model.align.submit.fasta\n")


def write_alignment_file(scratch_path, target_sequence, chain):
    """Create the ProModII alignment file"""
    t_offsets = dict()
    with open(
        os.path.join(scratch_path, "model.align.submit.fasta"), "w"
    ) as aln_fh:
        aln_fh.write("#SPDBV Foldfit-like Alignment\n")
        aln_fh.write(">Target\n")
        aln_fh.write(">EXPDB\n")
        aln_fh.write("*\n")

        tpl_seq = seq.CreateSequence(
            str(chain.name), "-" * len(target_sequence)
        )

        for res in chain.residues:
            tpl_seq[res.number.num - 1] = res.one_letter_code
        for i, trg in enumerate(target_sequence):
            aln_fh.write(" %s" % trg)
            if tpl_seq[i] != "-":
                aln_fh.write("%s\n" % tpl_seq[i])
            else:
                aln_fh.write(" \n")
        t_offsets[chain.name] = chain.residues[0].number.num - 1

        aln_fh.write("*\n")

    return t_offsets


def execute_promod(model_path, scratch_path, trg_seq, model_ost_ent):
    """Create a protein model using ProModII, including all the preparations.
    Calling ProModII requires some preparation. What we're interested in is raw
    modelling time by ProModII. This is returned by the execute_promod function.
    """
    write_batch_file(scratch_path, trg_seq)
    t_offsets = write_alignment_file(
        scratch_path, trg_seq, model_ost_ent.chains[0]
    )

    # execute
    poh = open(os.path.join(scratch_path, "promod.stdout"), "w")
    peh = open(os.path.join(scratch_path, "promod.stderr"), "w")
    cwd = os.getcwd()
    os.chdir(scratch_path)
    io.SavePDB(model_ost_ent, "raw-model.pdb")
    os.environ["SPDBV_USRSTUFF"] = "."
    os.environ["SPDBV_TEMP"] = "."
    os.environ["SPDBV_BASE"] = os.getenv("SPDBV_BASE")
    os.environ["SPDBV_DOWNLOAD"] = scratch_path
    os.environ["EXPDB"] = "/scicore/data/databases/EXPDB"
    command = (
        os.path.join(os.getenv("EBROOTPROMODII"), "bin", "promod")
        + " model.batch.1"
    )
    pmd_model_name = "model.batch.1_E.pdb"
    pm_modelling_time = 0.0
    try:
        pm_modelling_start_time = time.time()
        chld_ps = subprocess.Popen(command, shell=True, stdout=poh, stderr=peh)
        exit_code = chld_ps.wait()
        pm_modelling_time = time.time() - pm_modelling_start_time
        if exit_code != 0:
            raise RuntimeError(
                "ProMod did not finish properly, exit code: %d" % exit_code
            )
        if not os.path.exists(pmd_model_name):
            raise RuntimeError(
                "ProMod did not produce a model file (%s), " % pmd_model_name
                + "most likely it did not finish properly"
            )
    except Exception as exc:
        os.chdir(cwd)
        poh.close()
        peh.close()
        raise RuntimeError("ProMod failed: %s" % str(exc)) from exc
    # try to load the model before copying it to model.pdb. There are a few cases
    # where ProMod produces invalid PDB files. We should catch such errors and
    # report a failure.
    try:
        pmd_model = io.LoadPDB(pmd_model_name)

        # In case of oligo the chain are renamed by promod from A to Z
        # but the order is the same as in the raw model.
        # We need the original template names to preserve the mapping
        # of targets.
        pmd_model = rename_chains(pmd_model, [model_ost_ent.chains[0].name])

        # In case of oligomers, promod assigns ongoing residue numbers.
        # The first residue of the second chain will be numbered with the number
        # of the last residue of the first chain + 1. We have to renumber the
        # whole thing according to the seqres.
        renumber_chains(trg_seq, pmd_model, t_offsets)

    except Exception as exc:
        os.chdir(cwd)
        poh.close()
        peh.close()
        raise RuntimeError(
            "ProMod failed: Could not read model file: %s" % str(exc)
        ) from exc
    try:
        os.remove(pmd_model_name)
        os.chdir(cwd)
        io.SavePDB(pmd_model, model_path)
    except Exception as exc:
        os.chdir(cwd)
        poh.close()
        peh.close()
        raise RuntimeError(
            "ProMod failed: (renaming model file) %s" % str(exc)
        ) from exc
    os.chdir(cwd)
    poh.close()
    peh.close()
    return pm_modelling_time


if __name__ == "__main__":
    in_dir = "cameo_benchmark"
    out_dir = "promodII_models"
    scratch_dir = "scratch"
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if not os.path.exists(scratch_dir):
        os.makedirs(scratch_dir)

    targets = set(
        f[:6] for f in os.listdir(in_dir)
    )

    modelling_time = 0.0
    for t in sorted(targets):
        print("TARGET", t)
        tpl_path = os.path.join(in_dir, t + "_tpl.pdb")
        aln_path = os.path.join(in_dir, t + "_aln.fasta")
        out_path = os.path.join(out_dir, t + ".pdb")

        if os.path.exists(tpl_path) and os.path.exists(aln_path):
            aln = io.LoadAlignment(aln_path)
            template = io.LoadPDB(tpl_path).CreateFullView()
            aln.AttachView(1, template)
            rawmodel = promod3.modelling.BuildRawModel(aln)
            try:
                modelling_time += execute_promod(
                    out_path,
                    scratch_dir,
                    aln.GetSequence(0).GetGaplessString(),
                    rawmodel.model,
                )
            except Exception as e:
                print("FAIL: " + str(e))
            
    print("full modelling time: ", modelling_time)
