Scripts and data to reproduce various plots / tables from the Supporting 
information but also to reproduce the example modification of the modelling
pipeline which is described in the main manuscript.

1. S1 Fig: Length of modelled loops in homology modelling benchmark
2. S1 Table: Structural coverage in default StructureDB
3. Example modification of modelling pipeline


S1 Fig: Length of modelled loops in homology modelling benchmark
----------------------------------------------------------------

- fetch the stdout of running  build_promod_models.py in the modelling
  benchmark by executing 
  `pm build_promod_models.py > promod3_modelling_out.txt`
- copy promod3_modelling_out.txt in the same directory as 
  cameo_benchmark_loop_lengths.py
- execute `pm cameo_benchmark_loop_length.py` which creates 
  cameo_benchmark_loop_lengths.png and spits out some numbers

S1 Table: Structural coverage in default StructureDB
----------------------------------------------------

- execute `pm structural_coverage_in_StructureDB.py` to get the values that are 
  displayed in S1 Table. Be aware: the table reports the average values over 3 
  runs of this script. 

Example modification of modelling pipeline
------------------------------------------

Scripts, data and another README are available in the *mod_pipeline* directory.

