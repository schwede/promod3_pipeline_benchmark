import pandas as pd
import os
from ost import io
from promod3 import loop, modelling

data_table = pd.read_csv('data.csv')
profile = io.LoadSequenceProfile("BF.sm/tpl/hhm/query_hhblits.hhm")

for tpl_info in data_table.itertuples():
    print("processing", tpl_info.pdb_id, tpl_info.chain_name, tpl_info.pdb_file)
    tpl = io.LoadPDB(tpl_info.pdb_file)
    aln = io.LoadAlignment(tpl_info.aln_file)
    aln.AttachView(1, tpl.CreateFullView())
    mhandle = modelling.BuildRawModel(aln)
    modelling.SetSequenceProfiles(mhandle, [profile])
    final_model = modelling.BuildFromRawModel(mhandle)
    out_path = tpl_info.pdb_file

    out_path = os.path.join("models", os.path.basename(tpl_info.pdb_file))
    io.SavePDB(final_model, out_path)

