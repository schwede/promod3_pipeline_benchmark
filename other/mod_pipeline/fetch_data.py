from sm.pipeline import ProjectFactory
from sm.smtl import SMTL
import os

proj = ProjectFactory.FromName('BF.sm')
tpl_lib = SMTL('/scratch/smng_dbs/SMTL')

added_stuff = list()
stuff_for_csv = list()

for tpl_idx, tpl in enumerate(proj.templates):

    if tpl.seq_id > 90.0:
        continue

    if 'X-RAY' not in tpl.method:
        continue

    bu = tpl_lib.Get(tpl.biounit_name)
    chain = bu.GetChainByName(tpl.chain_name)

    stuff_id = '_'.join([str(tpl.biounit_name.split('.')[0]), str(chain.orig_pdb_name), str(tpl.found_by)])
    if stuff_id in added_stuff:
        continue
    added_stuff.append(stuff_id)

    seqres_aln = tpl.target_seqres_aln
    atom_aln = tpl.target_atom_aln
    offset = seqres_aln.GetSequence(1).offset
    start_rnum = offset + 1
    n_residues = len(str(seqres_aln.GetSequence(1)).replace('-', ''))
    end_rnum = start_rnum + n_residues - 1

    tmp = [str(tpl.biounit_name.split('.')[0]), str(chain.orig_pdb_name)]
    structure_out_path = os.path.join('data', stuff_id + '.pdb')
    aln_out_path = os.path.join('data', stuff_id + '_aln.fasta')
    seqres_out_path = os.path.join('data', stuff_id + '.fasta')
    profile_out_path = os.path.join('data', stuff_id + '.hhm')

    structure = chain.structure
    structure.EditXCS().RenameChain(structure.chains[0], str(chain.orig_pdb_name))
    structure = structure.Select('rnum=%i:%i'%(start_rnum,end_rnum))
    seqres = chain.entity.seqres
    profile_path = tpl_lib.ProfileForTemplate(chain.unique_id)

    io.SavePDB(structure, structure_out_path)
    io.SaveSequence(seqres, seqres_out_path)
    io.SaveAlignment(atom_aln, aln_out_path)
    os.system(' '.join(['cp', profile_path, profile_out_path]))

    stuff_for_csv.append('%s,%s,%s,%s,%s,%s'%(str(tpl.biounit_name.split('.')[0]),
                                              str(chain.orig_pdb_name),
                                              structure_out_path,
                                              seqres_out_path,
                                              aln_out_path,
                                              profile_out_path))

outfile = open('data.csv', 'w')
outfile.write('pdb_id,chain_name,pdb_file,seqres_file,aln_file,prof_file\n')
outfile.write('\n'.join(stuff_for_csv))
outfile.close()

