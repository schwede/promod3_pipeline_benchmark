from ost import io, mol
from promod3 import loop, modelling

# Load template and alignment
tpl = io.LoadPDB('data/4b8v_A_BLAST.pdb')
mol.alg.AssignSecStruct(tpl)
aln = io.LoadAlignment('data/4b8v_A_BLAST_aln.fasta')
aln.AttachView(1, tpl.CreateFullView())

# Modelling algorithms operate on a modelling handle
# Besides coordinates it tracks non-closed gaps etc.
mhandle = modelling.BuildRawModel(aln)

# Load custom databases and try to close gaps in mhandle
frag_db = loop.FragDB.Load('frag_db.dat')
struct_db = loop.StructureDB.Load('struct_db.dat')
modelling.FillLoopsByDatabase(mhandle, frag_db, struct_db)

# Invoke default modelling pipeline to model remaining gaps,
# sidechains and minimize model energy
final_model = modelling.BuildFromRawModel(mhandle)
io.SavePDB(final_model, 'model.pdb')

