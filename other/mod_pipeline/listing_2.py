import pandas as pd
from ost import io
from promod3 import loop, modelling

# Load csv file with file paths etc. of found templates
data_table = pd.read_csv('data.csv')

# Create StructureDB and fill with prepared data
struct_db = loop.StructureDB(loop.StructureDBDataType.Minimal)
for tpl in data_table.itertuples():
	coords = io.LoadPDB(tpl.pdb_file)
	seqres = io.LoadSequence(tpl.seqres_file)
	struct_db.AddCoordinates(tpl.pdb_id, tpl.chain_name,
                         	    coords, seqres)

# Create FragDB which refers to our StructureDB
frag_db = loop.FragDB(1.0, 20)
for i in range(3,15): # i defines length of added fragments
  frag_db.AddFragments(i, 1.0, struct_db)

# Save databases
frag_db.Save('frag_db.dat')
struct_db.Save('struct_db.dat')

