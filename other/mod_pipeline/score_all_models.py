import os
import json

target_path = 'target.pdb'
model_files = os.listdir('models')
results = list()
for mf in model_files:
    model_path = os.path.join('models', mf)
    os.system("ost compare-structures -m %s -r %s -o %s --lddt"%(model_path, 
                                                                 target_path, 
                                                                 'tmp.json'))
    data = json.load(open('tmp.json'))
    lddt_data = data['result'][mf][target_path]['lddt']
    results.append((lddt_data['single_chain_lddt'][0]['global_score'], mf))

for item in sorted(results, reverse=True):
    print(item[1], item[0])

