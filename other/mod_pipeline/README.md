Scripts and data to reproduce the results in the section 
'Example modification of modelling pipeline'.

It's a two step process

1. Fetch Data
2. Modelling and evaluation

Fetch Data
----------

We suggest to use the already provided data as you need a running SWISS-MODEL 
instance which is not publicly available. The steps are nevertheless documented 
here for internal reference.

- Fetch a SWISS-MODEL project with already executed template search specific to 
  our target sequence. *BF.sm* contains such a project.
- Execute `sm fetch_data.py` to extract templates, alignments and profiles. They 
  are dumped in *data* and all the info is summarized in *data.csv*.

Modelling and evaluation
------------------------

- In a first step we model the target with all available templates by executing
  `pm build_models_from_all_templates.py`. This reads the information in 
  *data.csv* and builds/dumps the models in *models*.
- Executing `pm score_all_models.py` compares all previously built models to 
  *target.pdb* and prints the respective lDDT scores.
- The custom modelling pipeline is defined in the two scripts denoted as 
  *listing_2.py* and *listing_3.py* which refer to the listings in the main 
  manuscript.
  Executing `pm listing_2.py` loads all templates in *data* and creates the 
  custom StructureDB and FragDB. The final model gets built with the custom 
  pipeline by executing `pm listing_3.py`.
- The lDDT score of the created *model.pdb* with respect to *target.pdb* can
  be estimated with `pm score_final_model.py`.

