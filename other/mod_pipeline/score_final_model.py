import os
import json

target_path = 'target.pdb'
model_path = 'model.pdb'

os.system("ost compare-structures -m %s -r %s -o %s --lddt"%(model_path, 
                                                             target_path, 
                                                             'tmp.json'))
data = json.load(open('tmp.json'))
lddt_data = data['result'][model_path][target_path]['lddt']
print("lDDT:", lddt_data['single_chain_lddt'][0]['global_score'])

