Scripts and data to reproduce the loop modelling accuracy benchmark.

The whole benchmark is a three step process:

1. Fetch Benchmark Set
2. Create Non-Redundant Databases
3. Modelling / Evaluation


Fetch Benchmark Set
-------------------

- Run `pm download_structures.py` - This reads the loop definitions in
  fread_data.txt and directly fetches the according files from RCSB.
  PDB files are dumped in the **structure** directory and the sequence
  for each chain is dumped in **sequences**.
- To run ProMod3 as we do in SWISS-MODEL, you also need sequence profiles in
  hhm format. You need for every file **xyz.fasta** in **sequences** an 
  equivalent named **xyz.hhm** in **profiles**. Either you run HHblits by
  yourself or you just go for the files we're providing to you. 


Create Non-Redundant Databases
------------------------------

- Run `pm create_non_redundant_structure_db.py` - Loads the default StructureDB
  and FragDB used for loop modelling and creates non-redundant versions of it.
  Entries with high sequence identity to any of the chains where we want
  to model a loop are removed. This takes some time...


Modelling / Evaluation
-----------------------------------

- Run `pm model_loops.py` - This reads the loop definitions in fread_data.txt
  and remodels the defined loops by using the prepared data.
  The average and median RMSD values (based on N, CA, C and O atoms) are 
  directly printed at the end of the script. For each loop length you get:

  - native RMSD values: Raw RMSD of the loop as it is remodelled towards the
    experimental coordinates.
  - superposed RMSD values: RMSD of the extracted loop upon minimum RMSD 
    superposition onto the experimental coordinates.
  - minimized RMSD values: The whole structure with the remodelled loop is
    energy minimized as you would process a model in the last step of the 
    default homology modelling pipeline in ProMod3. After that, RMSD values are 
    estimated the same way as the native RMSD values.

  Table 1 in the ProMod3 manuscript reports the per-length averages of the 
  minimized RMSD values.

