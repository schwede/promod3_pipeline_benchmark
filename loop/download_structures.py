import os
from ost import io, mol, seq
import promod3

# sometimes the modelling fails, because the structure contains residues with 
# olc 'U' / '?' lets simply replace them by alanine if all backbone atoms are 
# present.  
def Clean(prot):
    ed = prot.EditXCS()
    for r in prot.residues:
        if r.one_letter_code == 'U' or r.one_letter_code == '?':
            # check for presence of the backbone atoms
            n = r.FindAtom('N')
            ca = r.FindAtom('CA')
            c = r.FindAtom('C')
            o = r.FindAtom('O')
            # if everything is there we keep it as alanine and kick away
            # all non alanine atoms...
            if n.IsValid() and ca.IsValid() and c.IsValid() and o.IsValid():
                ed.RenameResidue(r,'ALA')
                r.SetOneLetterCode('A')
                for a in r.atoms:
                    aname = a.GetName()
                    if aname not in ['N', 'CA', 'C', 'O', 'CB']:
                        ed.DeleteAtom(a)
            else:
                ed.DeleteResidue(r)

structure_out_dir = 'structures'
sequence_out_dir = 'sequences'

# make sure we have a compound library
promod3.SetCompoundsChemlib()

if not os.path.exists(structure_out_dir):
    os.makedirs(structure_out_dir)
if not os.path.exists(sequence_out_dir):
    os.makedirs(sequence_out_dir)

with open('fread_data.txt') as fh:
  testset_data = fh.readlines()

for item in testset_data:
    pdb_id = item.split()[0]
    chain_id = item.split()[1]
    print('processing', pdb_id, chain_id)
    out_path = os.path.join(structure_out_dir, pdb_id + '.pdb')
    prot = io.LoadPDB(pdb_id, remote=True).Select('peptide=true')
    prot = mol.CreateEntityFromView(prot, True)
    Clean(prot)
    io.SavePDB(prot, out_path)
    for ch in prot.chains:
        s = ''.join([r.one_letter_code for r in ch.residues])
        s = seq.CreateSequence(ch.GetName(), s)
        s_out = os.path.join(sequence_out_dir, 
                             '_'.join([pdb_id, ch.GetName()]) + '.fasta')
        io.SaveSequence(s, s_out)

