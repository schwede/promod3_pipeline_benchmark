Scripts and data to reproduce the sidechain modelling accuracy benchmark.

The whole benchmark is a three step process:

1. Fetch Benchmark Set
2. Modelling with ProMod3 and SCWRL4
3. Evaluation

Fetch Benchmark Set
-------------------

Structures for benchmarking are fetched from the pdb using the remote loading
capabilities from OpenStructure:

`ost get_scwrl_testset_structures.py`

loads, cleans and saves structures to the scwrl_testset directory.


Modelling with ProMod3 and SCWRL4
---------------------------------

`pm reconstruct_sidechains_promod.py` reconstructs sidechains using different
different settings: 

- Flexible Rotamer Model (FRM) with subrotamer optimization with default 
  (backbone dependent) rotamer library
- FRM without subrotamer optimization with default rotamer library
- Rigid Rotamer Model (RRM) with default rotamer library
- FRM with subrotamer optimization and backbone indepenedent rotamer library
- RRM with backbone indepenedent rotamer library

The generated models appear in the subdirectories of models/promod

`python reconstruct_sidechains_scwrl.py` reconstructs sidechains with
the flexible rotamer model as well as the rigid rotamer model. The script
requires the Scwrl4 executable in your path and the generated models appear
in the subdirectories of models/scwrl

Evaluation
----------

`pm do_plot.py` recreates Fig 2 from ProMod3 manuscript.

`pm do_supplemental_tables.py` creates detailled csv tables with performances
of the alternative modelling runs from the last step.

