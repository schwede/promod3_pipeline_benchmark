import os, time
import subprocess

# make sure the appropriate directories exist
if not os.path.exists('models'):
  os.makedirs('models')

promod_model_path = os.path.join('models', 'promod')

with open('scwrl_testset_ids.txt') as fh:
    data = fh.readlines()
testset_ids = list()
for line in data:
    testset_ids += line.strip().split()

def DoIt(in_dir, out_dir, testset_ids, action_flags):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    for item in testset_ids:
        in_path = os.path.join(in_dir, item.strip() + '-molcked.pdb')
        out_path = os.path.join(out_dir,item.strip() + '.pdb')
        cmd = ['pm', 'build-sidechains', '-p', in_path, '-o', out_path, '-f', 'SCWRL4'] 
        cmd += action_flags
        subprocess.run(cmd)

##########
# DO FRM #
##########
print('Do ProMod3 FRM (default)')
out_dir = os.path.join(promod_model_path, 'frm')
start = time.time()
DoIt('scwrl_testset', out_dir, testset_ids, [])
print('overall time: ', time.time() - start)

##########################################
# DO FRM WITHOUT SUBROTAMER OPTIMIZATION #
##########################################
print('Do ProMod3 FRM Without subrotamer optimization')
out_dir = os.path.join(promod_model_path, 'frm_no_subrotamer_optimization')
start = time.time()
DoIt('scwrl_testset', out_dir, testset_ids, ['-s'])
print('overall time: ', time.time() - start)

##########
# DO RRM #
##########
print('Do ProMod3 RRM')
out_dir = os.path.join(promod_model_path, 'rrm')
start = time.time()
DoIt('scwrl_testset', out_dir, testset_ids, ['-r'])
print('overall time: ', time.time() - start)

###############################
# DO BACKBONE INDEPENDENT FRM #
###############################
print('Do ProMod3 BB Indep FRM')
out_dir = os.path.join(promod_model_path, 'bb_indep_frm')
start = time.time()
DoIt('scwrl_testset', out_dir, testset_ids, ['-i'])
print('overall time: ', time.time() - start)

###############################
# DO BACKBONE INDEPENDENT RRM #
###############################
print('Do ProMod3 BB Indep RRM')
out_dir = os.path.join(promod_model_path, 'bb_indep_rrm')
start = time.time()
DoIt('scwrl_testset', out_dir, testset_ids, ['-i', '-r'])
print('overall time: ', time.time() - start)

