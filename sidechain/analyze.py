
import os
import numpy as np
import traceback
from promod3 import sidechain
from ost import io, seq, geom, mol

def _GetAmbigRMSD(r_one, r_two, fix_atoms, ambig_atom_pairs):

    rmsd_one = 0.0
    rmsd_two = 0.0

    for aname in fix_atoms:
        a = r_one.FindAtom(aname)
        b = r_two.FindAtom(aname)
        if a.IsValid() and b.IsValid():
            dist = geom.Distance(a.GetPos(),b.GetPos())
            rmsd_one += dist*dist
            rmsd_two += dist*dist
        else:
            return float('NaN')

    # process rmsd_one: everything matches nicely
    for anames in ambig_atom_pairs:
        a = r_one.FindAtom(anames[0])
        b = r_two.FindAtom(anames[0])
        if not (a.IsValid() and b.IsValid()):
            return float('NaN')        
        dist = geom.Distance(a.GetPos(),b.GetPos())
        rmsd_one += dist*dist
        a = r_one.FindAtom(anames[1])
        b = r_two.FindAtom(anames[1])
        if not (a.IsValid() and b.IsValid()):
            return float('NaN')        
        dist = geom.Distance(a.GetPos(),b.GetPos())
        rmsd_one += dist*dist

    # process rmsd_two: the ambig atom pairs are flipped
    for anames in ambig_atom_pairs:
        a = r_one.FindAtom(anames[0])
        b = r_two.FindAtom(anames[1])
        if not (a.IsValid() and b.IsValid()):
            return float('NaN')        
        dist = geom.Distance(a.GetPos(),b.GetPos())
        rmsd_two += dist*dist
        a = r_one.FindAtom(anames[1])
        b = r_two.FindAtom(anames[0])
        if not (a.IsValid() and b.IsValid()):
            return float('NaN')        
        dist = geom.Distance(a.GetPos(),b.GetPos())
        rmsd_two += dist*dist

    n_atoms = len(fix_atoms) + 2 * len(ambig_atom_pairs)
    return min(np.sqrt(rmsd_one / n_atoms), np.sqrt(rmsd_two / n_atoms))



def GetRMSD(r_one, r_two):

    rname = r_one.GetName()
    if rname != r_two.GetName():
        return float('NaN')
    # handle the ambiguous cases
    if rname == 'TYR':
        return _GetAmbigRMSD(r_one, r_two, ['CG', 'CZ', 'OH'], 
                             [('CD1', 'CD2'), ('CE1', 'CE2')])
    if rname == 'VAL':
        return _GetAmbigRMSD(r_one, r_two, [], [('CG1', 'CG2')])
    if rname == 'PHE':
        return _GetAmbigRMSD(r_one, r_two, ['CG', 'CZ'],
                             [('CD1', 'CD2'), ('CE1', 'CE2')])
    if rname == 'ASP':
        return _GetAmbigRMSD(r_one, r_two, ['CG'], [('OD1', 'OD2')])
    if rname == 'GLU':
        return _GetAmbigRMSD(r_one, r_two, ['CG', 'CD'], [('OE1', 'OE2')])
    # the general case
    rm = 0.0
    counter = 0
    for a in r_one.atoms:
        b = r_two.FindAtom(a.GetName())
        if a.IsValid() and b.IsValid():
            dist = geom.Distance(a.GetPos(),b.GetPos())
            rm += dist*dist
            counter+=1
        else:
            return float('NaN')
    return np.sqrt(rm/counter)


class AAEval:
  def __init__(self):
    self.num_valid_chi1 = 0.0
    self.num_correct_chi1 = 0.0
    self.num_valid_chi2 = 0.0
    self.num_correct_chi2 = 0.0
    self.num_valid_chi2_given_chi1 = 0.0
    self.num_correct_chi2_given_chi1 = 0.0
    self.rmsd_values = list()
    self.num_residues = 0.0
    self.num_clashing = 0.0


def GetAAEvaluations(model_dir, target_dir = 'scwrl_testset', 
                     angle_thresh =  20.0/180.0*np.pi):

    AA = ['ARG','ASN','ASP','CYS','GLN','GLU','HIS','ILE','LEU','LYS','MET','PHE',
          'PRO','SER','THR','TRP','TYR','VAL']
    eval_data = {aa : AAEval() for aa in AA}
    testset_ids = set(f[:4] for f in os.listdir(target_dir))
    ff = mol.mm.LoadCHARMMForcefield()
    for target_idx, t in enumerate(testset_ids):
        try:
            target_path = os.path.join(target_dir, t + '-molcked.pdb')
            model_path = os.path.join(model_dir, t + '.pdb')
            target_structure = io.LoadPDB(target_path).Select('peptide=true')
            model_structure = io.LoadPDB(model_path).Select('peptide=true')
            # If several chains, we only consider unique sequences of chains
            # => first occurence matters (e.g. in case of homo oligomers)
            present_chains = list()
            present_sequences = list()
            for ch in target_structure.chains:
                ch_s = seq.CreateSequence('A', ''.join([r.one_letter_code for r in ch.residues]))
                already_there = False
                for s in present_sequences:
                    aln = seq.alg.GlobalAlign(ch_s, s, seq.alg.BLOSUM62)[0]
                    s_id = seq.alg.SequenceIdentity(aln)
                    if s_id > 0.9:
                        # High sequence identity can be caused through a supper crappy 
                        # alignment where we mostly have gaps. Lets estimate the fraction 
                        # of aligned residues for the shorter sequence
                        s0 = str(aln.GetSequence(0))
                        s1 = str(aln.GetSequence(1))
                        n_aligned = sum([int(a!='-' and b!='-') for a,b in zip(s0, s1)])
                        frac = float(n_aligned) / min(sum([int(c!='-') for c in s0]),
                                                      sum([int(c!='-') for c in s1]))
                        # set arbitrary threshold, most residues are expected to be aligned
                        if frac > 0.8:
                            already_there = True
                if not already_there:
                    present_chains.append(ch.GetName())
                    present_sequences.append(ch_s)
            relevant_query = 'cname=' + ','.join(present_chains)
            relevant_target = target_structure.Select(relevant_query)
            residues_to_consider = relevant_target.residues
            for r_t in residues_to_consider:
                rot_id = sidechain.TLCToRotID(r_t.GetName())
                if rot_id == sidechain.XXX or rot_id == sidechain.GLY or rot_id == sidechain.ALA:
                    continue
                r_m = model_structure.FindResidue(r_t.GetChain().GetName(),r_t.GetNumber())
                if not r_m.IsValid():
                    continue
                try:
                    rot_t = sidechain.RotamerLibEntry.FromResidue(r_t)
                    rot_m = sidechain.RotamerLibEntry.FromResidue(r_m)
                except:
                    continue # one of the residues is incomplete
                eval_data[r_t.GetName()].num_residues += 1
                t_chi1 = rot_t.chi1
                m_chi1 = rot_m.chi1
                t_chi2 = rot_t.chi2
                m_chi2 = rot_m.chi2
                chi1_ok = rot_t.SimilarDihedral(rot_m, 0, angle_thresh, rot_id)
                chi2_ok = rot_t.SimilarDihedral(rot_m, 1, angle_thresh, rot_id)
                if t_chi1 == t_chi1 and m_chi1 == m_chi1:
                    eval_data[r_t.GetName()].num_valid_chi1 += 1
                    if chi1_ok:
                        eval_data[r_t.GetName()].num_correct_chi1 += 1
                if t_chi2 == t_chi2 and m_chi2 == m_chi2:
                    eval_data[r_t.GetName()].num_valid_chi2 += 1
                    if chi2_ok:
                        eval_data[r_t.GetName()].num_correct_chi2 += 1
                if chi1_ok:
                    if t_chi2 == t_chi2 and m_chi2 == m_chi2:
                        eval_data[r_t.GetName()].num_valid_chi2_given_chi1 += 1
                        if chi2_ok:
                            eval_data[r_t.GetName()].num_correct_chi2_given_chi1 += 1
                rm = GetRMSD(r_t, r_m)
                if rm == rm:
                    eval_data[r_t.GetName()].rmsd_values.append(rm)


            # clash analysis is done at the very end as we need to do some
            # renaming magic...
            mol.mm.MMModeller.AssignGromacsNaming(model_structure.handle)
            ff.AssignFFSpecificNames(model_structure.handle)
            model_structure = model_structure.Select('peptide=true and ele!=H')
            relevant_model_structure = model_structure.Select(relevant_query)
            # we only count clashes of residues from unique chains but do that
            # relative to the whole structure
            for r in relevant_model_structure.residues:
                residue_clashes = False
                for a in r.atoms:
                    if residue_clashes:
                        break
                    if a.GetName() in ['N', 'CA', 'C', 'O', 'CB']:
                        continue # we're only interested in sidechain atoms
                    a_type = ff.GetAtomType(r.name, a.name)
                    close_atoms = model_structure.FindWithin(a.GetPos(), 4.0)
                    for b in close_atoms:
                        if a.residue.GetQualifiedName() != \
                           b.residue.GetQualifiedName():
                            if a.name == 'SG' and b.name == 'SG':
                                continue # skip disulfid bonds
                            b_type = ff.GetAtomType(b.residue.name, b.name)
                            lj = ff.GetLJ(a_type, b_type, False)
                            sigma = lj.GetParam()[0]
                            sigma *= 10 # nm -> A
                            d = geom.Distance(a.GetPos(), b.GetPos())
                            if d <= 0.6*sigma:
                                residue_clashes = True
                                break
                if residue_clashes:
                    # we first need to undo renaming mess
                    rname = r.GetName()
                    if rname == "HSE":
                        rname = "HIS"
                    eval_data[rname].num_clashing += 1

        except:
            print('failed to evaluate target', t)
            traceback.print_exc()

    return eval_data


def PrintPerformance(model_dir, only_table=True, csv_path=None):

    eval_data = GetAAEvaluations(model_dir)

    # get the total number of chi1 values
    total_num_chi1 = 0
    total_num_chi2 = 0
    total_num_residues = 0
    total_correct_num_chi1 = 0
    total_correct_num_chi2 = 0
    total_num_clashing = 0
    for key in eval_data.keys():
        total_num_chi1 += eval_data[key].num_valid_chi1
        total_num_chi2 += eval_data[key].num_valid_chi2
        total_num_residues += eval_data[key].num_residues
        total_correct_num_chi1 += eval_data[key].num_correct_chi1
        total_correct_num_chi2 += eval_data[key].num_correct_chi2
        total_num_clashing += eval_data[key].num_clashing

    if not only_table:
        print('total num valid chi1 angles: ', total_num_chi1)
        print('fraction correct:', float(total_correct_num_chi1) / total_num_chi1)
        print('total num valid chi2 angles: ', total_num_chi2)
        print('fraction correct:', float(total_correct_num_chi2) / total_num_chi2)
        print('total num clashing', total_num_clashing)
        print('total num residues: ', total_num_residues)

        for key in eval_data.keys():
            print(key)
            if eval_data[key].num_valid_chi1 > 0:
                print('chi1 accuracy: ', 
                      eval_data[key].num_correct_chi1/eval_data[key].num_valid_chi1, 
                      eval_data[key].num_valid_chi1, eval_data[key].num_residues)
            if eval_data[key].num_valid_chi2 > 0:
                print('chi2 accuracy: ',
                      eval_data[key].num_correct_chi2/eval_data[key].num_valid_chi2,
                      eval_data[key].num_valid_chi2, eval_data[key].num_residues)
            if eval_data[key].num_valid_chi2_given_chi1 > 0:
                print('chi2 accuracy given chi1: ',
                      eval_data[key].num_correct_chi2_given_chi1/eval_data[key].num_valid_chi2_given_chi1,
                      eval_data[key].num_valid_chi2_given_chi1)
            print("num clashing: ", eval_data[key].num_clashing)

    # output for csv table
    csv_out = list()
    csv_out.append('AA,num,X1 correct (%),X2 correct (%),X2 correct given X1 (%),avg RMSD, n_clash')
    AA = ['ARG','ASN','ASP','CYS','GLN','GLU','HIS','ILE','LEU','LYS','MET','PHE','PRO','SER','THR','TRP','TYR','VAL']
    for aa in AA:
        data = eval_data[aa]
        num = '%i'%(int(data.num_valid_chi1))
        chi1_fraction = ' '
        chi2_fraction = ' ' 
        chi2_fraction_given_chi1 = ' '
        rmsd = ' '
        n_clash = str(int(data.num_clashing))
        if data.num_valid_chi1 > 0:
            chi1_fraction = '%.2f'%(100*float(data.num_correct_chi1)/data.num_valid_chi1)
        if data.num_valid_chi2 > 0:
            chi2_fraction = '%.2f'%(100*float(data.num_correct_chi2)/data.num_valid_chi2)
        if(data.num_valid_chi2_given_chi1) > 0:
            chi2_fraction_given_chi1 = '%.2f'%(100*data.num_correct_chi2_given_chi1/data.num_valid_chi2_given_chi1)
        if len(data.rmsd_values) > 0:
            rmsd = '%.2f'%(np.mean(data.rmsd_values))
        csv_out.append(','.join([aa, num, chi1_fraction, chi2_fraction, chi2_fraction_given_chi1, rmsd, n_clash]))
  
    if csv_path:
        with open(csv_path, 'w') as fh:
            fh.write('\n'.join(csv_out))
