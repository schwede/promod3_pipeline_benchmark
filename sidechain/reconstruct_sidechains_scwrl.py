import os
import subprocess
import traceback
import time

# make sure the appropriate directories exist
if not os.path.exists('models'):
    os.makedirs('models')

scwrl_model_path = os.path.join('models', 'scwrl')
if not os.path.exists(scwrl_model_path):
    os.makedirs(scwrl_model_path)

# read the testset ids
file_content = open('scwrl_testset_ids.txt').readlines()
testset_ids = list()
for line in file_content:
  split_line = line.strip().split()
  for item in split_line:
    testset_ids.append(item)

##########
# DO FRM #
##########
out_dir = os.path.join(scwrl_model_path, 'frm')
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
print('Do SCWRL4 FRM')
start = time.time()
for item in testset_ids:
    in_path = os.path.join('scwrl_testset', item.strip() + '-molcked.pdb')
    out_path = os.path.join(out_dir, item.strip() + '.pdb')
    cmd = ['Scwrl4', '-i', in_path, '-o', out_path]
    subprocess.run(cmd)
print('overall time: ', time.time() - start)

##########
# DO RRM #
##########
out_dir = os.path.join(scwrl_model_path, 'rrm')
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
print('Do SCWRL4 RRM')
start = time.time()
for item in testset_ids:
    in_path = os.path.join('scwrl_testset', item.strip() + '-molcked.pdb')
    out_path = os.path.join(out_dir, item.strip() + '.pdb')
    cmd = ['Scwrl4', '-v', '-i', in_path, '-o', out_path]
    subprocess.run(cmd)
print('overall time: ', time.time() - start)

