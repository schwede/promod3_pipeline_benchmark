import analyze

model_directories = ['models/promod/frm',
                     'models/promod/frm_no_subrotamer_optimization',
                     'models/promod/rrm',
                     'models/promod/bb_indep_frm',
                     'models/promod/bb_indep_rrm',
                     'models/scwrl/frm',
                     'models/scwrl/rrm']


# you'll get a different number of evaluated aspartic acids in this testset when
# evaluating SCWRL4 and ProMod3
# The bad guy is: 2pst, X.ASP9
# The problem is: a backbone oxygen is missing.
# SCWRL4 completely deletes the residue
# ProMod3 doesn't touch it and the correct sidechain remains in place
# This is unfair in favour of ProMod3, as the correct sidechain is evaluated.
# however, the effect on overall performance can considered to be small...

for md in model_directories:
    print('processing:', md)
    csv_path = '_'.join(['eval', md.split('/')[-2], md.split('/')[-1] + '.csv']) 
    analyze.PrintPerformance(md, False, csv_path=csv_path)

