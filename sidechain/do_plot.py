import matplotlib.pyplot as plt
import numpy as np
import analyze

model_dir_one = 'models/promod/frm'
model_dir_two = 'models/scwrl/frm'

label_one = 'ProMod3'
label_two = 'SCWRL4'

fig_out_path = 'chi1_accuracy_frm.png'

one = analyze.GetAAEvaluations(model_dir_one)
two = analyze.GetAAEvaluations(model_dir_two)

one_chi1 = list()
two_chi1 = list()
AA = ['ARG','ASN','ASP','CYS','GLN','GLU','HIS','ILE','LEU','LYS','MET','PHE',
      'PRO','SER','THR','TRP','TYR','VAL']
for aa in AA:
    one_chi1.append(one[aa].num_correct_chi1/one[aa].num_valid_chi1)
    two_chi1.append(two[aa].num_correct_chi1/two[aa].num_valid_chi1)

x_promod = list()
x_scwrl = list()
for i in range(18):
    x_promod.append(i+0.1)
    x_scwrl.append(i+0.5)

cred = (128.0/255,0.0,0.0)
cblue = (102.0/255,153.0/255,204.0/255)
plt.bar(x_promod, one_chi1, width = 0.4, color=cred, label=label_one, 
        linewidth=2.0, alpha=0.75, align='edge', edgecolor='k')
plt.bar(x_scwrl, two_chi1, width = 0.4, color=cblue, label=label_two, 
        linewidth=2.0, alpha=0.75, align='edge', edgecolor='k')
plt.ylim((0.5, 1.0))
plt.legend(frameon=False, loc='upper left')
x_tick_positions = list()
for item in x_promod:
  x_tick_positions.append(item + 0.4)
plt.xticks(x_tick_positions, AA, rotation=30)
plt.ylabel(r'$X_1$: Fraction correct', fontsize='x-large')
plt.savefig(fig_out_path)
plt.clf()
