import os
from ost.mol.alg import MolckSettings, Molck
from ost import conop, io
import traceback

with open('scwrl_testset_ids.txt') as fh:
    data = fh.readlines()
testset_ids = list()
for line in data:
    testset_ids += line.strip().split()

outdir = 'scwrl_testset'
if not os.path.exists(outdir):
    os.makedirs(outdir)

compound_lib = conop.GetDefaultLib()
molck_settings = MolckSettings(rm_unk_atoms=True,
                               rm_non_std=True,
                               rm_hyd_atoms=True,
                               rm_oxt_atoms=True,
                               rm_zero_occ_atoms=True,
                               colored=False,
                               map_nonstd_res=True,
                               assign_elem=True)

for item in testset_ids:
    try:
        print('processing ', item)
        out_path = os.path.join(outdir, item + '.pdb')  
        prot = io.LoadPDB(item, remote = True)
        io.SavePDB(prot.Select('peptide=true'), out_path)
        molcked_out_path = os.path.join(outdir, item + '-molcked.pdb')
        Molck(prot, compound_lib, molck_settings)
        io.SavePDB(prot.Select('peptide=true'), molcked_out_path)
    except:
        print('failed for', item)
        traceback.print_exc()

